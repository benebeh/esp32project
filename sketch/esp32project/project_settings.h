#ifndef __PROJECT_SETTINGS_H__
#define __PROJECT_SETTINGS_H__

#include <ESP32Project.h>

void changeSettings()
{
  model::SettingsInput projectSettings;
  projectSettings.longTermMeasureDelay = 1000*60*10;
  projectSettings.shortTermMeasureDelay = 1000*10;
  projectSettings.sps30Address = 0x69; // I2C
  projectSettings.stateHandlerButtonPin = 2;
  projectSettings.sps30ProtocolType = model::UART;
  projectSettings.defaultMeasurementType = model::ShortTerm;
  projectSettings.useStateHandler = false;
  projectSettings.tickDelay = 1000;
  projectSettings.dhtPin = 32; 
  projectSettings.sps30UARTRx = 36;
  projectSettings.sps30UARTTx = 4;
  
  
  model::Settings::changeSettings(projectSettings);  
}




#endif // __PROJECT_SETTINGS_H__
/*
 * Sensor
 * Betrachtung beschriftete Seite von links nach recht
 * blau PIN 1 power
 * lila pin 2 data 
 * GRAU pin 3 GND
 * WEISS pin 4 GND
 * 
 * 
 * Arduino
 * blau  auf EXT1 pin 1(5V) oder PIN2(3.3V)
 * lila pin 2 data (digital pin port auswählen)
 * GRAU pin 3 GND (pin 3) oder auf UEXT  pin4
 * WEISS pin 4 GND same lol wie grau
 * 
 */
