#include "project_settings.h"
#include <ESP32Project.h>

using namespace model;

MeasurementType lastMeasurementType; 
unsigned long lastMeasurement = 0;
bool firstRun = true;

void setup() {
  Serial.begin(115200);
  Serial.println("Initializing whole project, this can take a while ...");

  changeSettings();
  
  // put your setup code here, to run once:
  // Gather instances
  Settings::Instance();
  Sps30Handler::Instance();
  SDHandle::Instance();
  EspRTC::Instance();
  StateHandler::Instance();
  DHTHandler::Instance();

  Serial.println("Running in " + String(Settings::Instance().defaultMeasurementType() == LongTerm ? "LongTerm" : "ShortTerm") + " mode");
}

void loop() {
  Sps30Handler& sps30 = Sps30Handler::Instance();
  SDHandle& sd = SDHandle::Instance();
  EspRTC& rtc = EspRTC::Instance();
  StateHandler& button = StateHandler::Instance();
  DHTHandler& dht = DHTHandler::Instance();
  const Settings& settings = Settings::Instance();
  
  lastMeasurementType = button.getCurrentState();

  while(1)
  {
    // aktuellen Status holen. Wenn kein Button angeschlossen ist: Default-Status nehmen.
    MeasurementType currentState = button.getCurrentState();

    // Höhe des Delays ermitteln, der nach einer Messung durchlaufen werden soll.
    unsigned minDelay = currentState == LongTerm ? settings.longTermMeasureDelay() : settings.shortTermMeasureDelay();

    // Sollte sich die Art der Messung geändert haben, sollte sofort gemessen werden.
    if(lastMeasurementType != currentState) 
    {
      lastMeasurement = 0;
      lastMeasurementType = currentState;  
    }

    // Wenn noch nicht genug Zeit abgelaufen ist: Noch mal warten
    if(millis() - lastMeasurement < minDelay && !firstRun)
    {
      delay(settings.tickDelay());
      continue;
    }

    firstRun = false;

    Serial.println("Time for another measurement round!");
    PMOutput measurementResult = sps30.read();
    DHTOutput dhtResult = dht.read();

    // Prüfen, ob auch richtig gemessen wurde
    if(measurementResult.isFilled())
    {
      Serial.println("Writing sps30 result to csv!");
      if(currentState == LongTerm)
      {
        LongTermOutputHandler(measurementResult).write();
      }
      else 
      {
        ShortTermOutputHandler(measurementResult).write();
      }
    }
    else 
    {
      Serial.println("No data in measurement result! I'll try again within the next tick!");
    }

    // Prüfen, ob auch richtig gemessen wurde
    if(dhtResult.isFilled())
    {
      Serial.println("Writing sps30 result to csv!");
      if(currentState == LongTerm)
      {
        LongTermOutputHandler(dhtResult).write();
      }
      else 
      {
        ShortTermOutputHandler(dhtResult).write();
      }

      lastMeasurement = millis();
    }
    else 
    {
      Serial.println("No data in measurement result! I'll try again within the next tick!");
    }

    delay(settings.tickDelay());
  }
}
