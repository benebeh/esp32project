#include "SDHandle.h"
#include <string.h>
#include <limits.h>     /* PATH_MAX */
#include <sys/stat.h>   /* mkdir(2) */
#include <errno.h>

// Define CS pin for the SD card module
#define SD_CS 5

namespace model 
{
	namespace 
	{
		int mkdir_p_native(const char *path)
		{
			/* Adapted from http://stackoverflow.com/a/2336245/119527 */
			const size_t len = strlen(path);
			char _path[PATH_MAX];
			char *p; 

			errno = 0;

			/* Copy string so its mutable */
			if (len > sizeof(_path)-1) {
				errno = ENAMETOOLONG;
				return -1; 
			}   
			strcpy(_path, path);

			/* Iterate the string */
			for (p = _path + 1; *p; p++) {
				if (*p == '/') {
					/* Temporarily truncate */
					*p = '\0';

					if (mkdir(_path, S_IRWXU) != 0) {
						if (errno != EEXIST)
							return -1; 
					}

					*p = '/';
				}
			}   

			if (mkdir(_path, S_IRWXU) != 0) {
				if (errno != EEXIST)
					return -1; 
			}   

			return 0;
		}

		void mkdir_p(const String& path)
		{
			int lastSlash = path.lastIndexOf("/");
			int firstSlash = path.indexOf("/");
			if(lastSlash == -1) return;

			if(lastSlash != firstSlash)
			{
				mkdir_p(path.substring(0, path.lastIndexOf("/", lastSlash-1)+1));
			}
			
			SD_MMC.mkdir(path.substring(0, lastSlash+1));
		}
	}

	struct SDHandle::SPrivateData
	{
		
		bool initialized = false;
		/*
		sdmmc_host_t host;
		sdmmc_card_t* card;

		SPrivateData(): host(SDMMC_HOST_DEFAULT()) {}
		*/
	};

	SDHandle& SDHandle::Instance()
	{
		static SDHandle ret;
		return ret;
	}

	SDHandle::SDHandle()
		: mData(new SPrivateData())
	{
		SD_MMC.begin();  
		if(!SD_MMC.begin()) {
			Serial.println("Card Mount Failed");
			return;
		}
		/*
		// To use 1-line SD mode, uncomment the following line:
		mData->host.flags = SDMMC_HOST_FLAG_1BIT;
		mData->host.max_freq_khz = SDMMC_FREQ_PROBING;

		// This initializes the slot without card detect (CD) and write protect (WP) signals.
		// Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
		sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();

		// Options for mounting the filesystem.
		// If format_if_mount_failed is set to true, SD card will be partitioned and formatted
		// in case when mounting fails.
		esp_vfs_fat_sdmmc_mount_config_t mount_config = {
			.format_if_mount_failed = true,
			.max_files = 5
		};

		esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &(mData->host), &slot_config, &mount_config, &(mData->card));
		if (ret != ESP_OK) {
			if (ret == ESP_FAIL) {
				Serial.println("Failed to mount filesystem. If you want the card to be formatted, set format_if_mount_failed = true.");
			} else {
				Serial.println("Failed to initialize the card. Make sure SD card lines have pull-up resistors in place.");
				Serial.print("Problem: ");
				Serial.println(ret);
			}
			return;
		} 
		else 
		{
			Serial.println("Successfully mounted SD on /sdcard");
			mData->initialized = true;
		}
		*/
	}


	SDHandle::~SDHandle()
	{
		//esp_vfs_fat_sdmmc_unmount();
		//Serial.println("Card unmounted");
	}

	void SDHandle::saveData(const String& text, const String& filePath, bool overwrite)
	{
		if (!SD_MMC.begin()) {
			Serial.println("ERROR - SD card initialization failed!");
			return;    // init failed
		}

		String fullPath = filePath;
		mkdir_p(fullPath);
		
		File file = SD_MMC.open(fullPath, overwrite ? FILE_WRITE : FILE_APPEND);
		if(!file)
		{
			file = SD_MMC.open(fullPath, FILE_WRITE);
			if(!file)
			{
				Serial.print("Kann Pfad " + fullPath + " nicht oeffnen!");
				return;
			}
		}

		if(!file.print(text + "\n"))
		{
			Serial.println("Konnte nicht in Pfad " + fullPath + " schreiben!" );
		}
		else 
		{
			Serial.println("Habe " + text + " nach " + fullPath + " geschrieben!");
		}

		file.close();

		/*
		if(!mData->initialized) 
		{
			Serial.println("SD-Karte nicht korrekt gemounted, breche ab!");
			return;
		}

		Serial.print("Speichere: ");
		Serial.println(text);
		Serial.print("Pfad: ");
		Serial.println(filePath);

		String fullPath = "/sdcard/" + filePath;
		mkdir_p(fullPath.c_str());
		FILE* file = fopen(fullPath.c_str(), overwrite ? "w" : "a");
		if(!file) 
		{
			Serial.println("Fehler beim Oeffnen der Datei!");
			return;
		}

		fprintf(file, "%s\n", text);
		fclose(file);

		Serial.println("Habe folgendes geschrieben:");
		Serial.println(text);
		*/
	}

	String SDHandle::readData(const String& filePath)
	{
		String ret, line;

		if (!SD_MMC.begin()) {
			Serial.println("ERROR - SD card initialization failed!");
			return ret;    // init failed
		}

		File file = SD_MMC.open(filePath);

		if(!file) 
		{
			Serial.println("Konnte Datei " + filePath + " nicht oeffnen!");
			return ret;
		}

		while(file.available())
		{
			line = file.readStringUntil('\n');
			ret += ret.length() > 0 ? "\n" : "";
			ret += line;
		}

		file.close();
		return ret;

		/*
		if(!mData->initialized) 
		{
			Serial.println("SD-Karte nicht korrekt gemounted, breche ab!");
			return String("");
		}

		FILE* file = fopen(String("/sdcard/" + filePath).c_str(), "r");
		char* content;
		long fsize;

		fseek(file, 0, SEEK_END);
		fsize = ftell(file);
		fseek(file, 0, SEEK_SET);
		content = (char*) malloc(fsize+1);

		fread(content, 1, fsize, file);
		content[fsize] = '\0';

		return String(content);
		*/
	}
}

