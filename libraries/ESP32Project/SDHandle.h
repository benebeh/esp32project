#ifndef __SDHANDLE_H__
#define __SDHANDLE_H__

#include <string>
#include <memory>

// Libraries for SD card (ESP32 POE)
#include "FS.h"
#include "SD_MMC.h"  //Change SD_MMC.cpp
#include <SPI.h>

namespace model
{
	class SDHandle
	{
	public:
		static SDHandle& Instance();

		~SDHandle();

		SDHandle(const SDHandle& other) = delete;
		SDHandle(SDHandle&& other) = delete;
		SDHandle& operator=(const SDHandle& other) = delete;
		SDHandle& operator=(SDHandle&& other) = delete;

		void saveData(const String& data, const String& path, bool overwrite);
		String readData(const String& filePath);

	private:
		struct SPrivateData;
		std::unique_ptr<SPrivateData> mData;

	private:
		SDHandle();
		void createIfNotExists(const String& path) const;

	};
}

#endif // __SDHANDLE_H__
