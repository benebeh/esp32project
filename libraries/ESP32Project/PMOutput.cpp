#include "PMOutput.h"

namespace model 
{
    struct PMOutput::SPrivateData
    {
        DateTime timestamp;

        SMassConcentrationData massConcentration;
        SQuantityConcentrationData quantityConcentration;

        bool dateTimeFilled = false;
        bool massFilled = false;
        bool quantityFilled = false;

        SPrivateData() {}
        SPrivateData(const SPrivateData& other):timestamp(other.timestamp),massConcentration(other.massConcentration),quantityConcentration(other.quantityConcentration) {}
    };

    PMOutput::PMOutput()
        : mData(new SPrivateData())
    {

    }

    PMOutput::PMOutput(const PMOutput& other)
        : mData(new SPrivateData(*(other.mData)))
    {
    }

    PMOutput::PMOutput(PMOutput&& other)
        : mData(std::move(other.mData))
    {
    }

    PMOutput::~PMOutput()
    {
        
    }

    PMOutput& PMOutput::operator=(const PMOutput& other)
    {
        mData.reset(new SPrivateData(*(other.mData)));

        return *this;
    }

    PMOutput& PMOutput::operator=(PMOutput&& other)
    {
        mData = std::move(other.mData);

        return *this;
    }

    DynamicJsonDocument PMOutput::toJson() const
    {
        DynamicJsonDocument ret(1024);
        JsonObject  root = ret.to<JsonObject>();
        JsonObject  data = ret.createNestedObject("data"), 
                    mass = data.createNestedObject("mass"), 
                    quantity = data.createNestedObject("quantity");

        String date = EspRTC::dateTimeToString(timestamp());

        root["timestamp"] = date;
        root["type"] = "sps30";
        
        mass["pm1d0"] = massConcentrationData().pm1;
        mass["pm2d5"] = massConcentrationData().pm2d5;
        mass["pm4"]   = massConcentrationData().pm4;
        mass["pm10"]  = massConcentrationData().pm10;

        quantity["pm0d5"] = quantityConcentrationData().pm0d5;
        quantity["pm1d0"] = quantityConcentrationData().pm1;
        quantity["pm2d5"] = quantityConcentrationData().pm2d5;
        quantity["pm4"]   = quantityConcentrationData().pm4;
        quantity["pm10"]  = quantityConcentrationData().pm10;

        return ret;
    }

    String PMOutput::toString() const 
    {
        String ret;
        
        if(outputFormat() == JSON) 
        {
            DynamicJsonDocument buffer(toJson());

            serializeJson(buffer, ret);
        }
        else 
        {
            std::vector<String> output = { "sps30", EspRTC::dateTimeToString(timestamp()), String(massConcentrationData().pm1), 
                                           String(massConcentrationData().pm2d5), String(massConcentrationData().pm4),
                                           String(massConcentrationData().pm10), String(quantityConcentrationData().pm0d5), 
                                           String(quantityConcentrationData().pm1), String(quantityConcentrationData().pm2d5),
                                           String(quantityConcentrationData().pm4), String(quantityConcentrationData().pm10)};

            for(const auto& str : output)
            {
                ret += ret.length() > 0 ? ";" : "";
                ret += str;
            }
        }

        return ret;
    }

    void PMOutput::setTimestamp(const DateTime& timestamp)
    {
        mData->timestamp = timestamp;
        mData->dateTimeFilled = true;
    }

	const DateTime& PMOutput::timestamp() const
    {
        return mData->timestamp;
    }

	void PMOutput::setMassConcentrationData(const SMassConcentrationData& data)
    {
        mData->massConcentration = data;
        mData->massFilled = true;
    }

	const SMassConcentrationData& PMOutput::massConcentrationData() const
    {
        return mData->massConcentration;
    }

	void PMOutput::setQuantityConcentrationData(const SQuantityConcentrationData& data)
    {
        mData->quantityConcentration = data;
        mData->quantityFilled = true;
    }

	const SQuantityConcentrationData& PMOutput::quantityConcentrationData() const
    {
        return mData->quantityConcentration;
    }

    bool PMOutput::isFilled() const 
    {
        return mData->dateTimeFilled && mData->massFilled && mData->quantityFilled;
    }
}
