#ifndef __DHTHANDLER_H__
#define __DHTHANDLER_H__

#include "DHTOutput.h"
#include "DHT.h"
#include "Settings.h"

namespace model
{
    class DHTHandler
    {
    public: 
        static DHTHandler& Instance();

        ~DHTHandler();
        DHTHandler(const DHTHandler& other) = delete;
		DHTHandler(DHTHandler&& other) = delete;
		DHTHandler& operator=(const DHTHandler& other) = delete;
		DHTHandler& operator=(DHTHandler&& other) = delete;

        DHTOutput read();

    private:
        DHTHandler();

        struct SPrivateData;
        std::unique_ptr<SPrivateData> mData;
    };
}

#endif // __DHTHANDLER_H__
