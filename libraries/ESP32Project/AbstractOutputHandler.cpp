#include "AbstractOutputHandler.h"

namespace model
{
	namespace
	{
		const String gBASE_PATH = "pms/";
	}

	struct AbstractOutputHandler::SPrivateData
	{
		AbstractOutput output;

		SPrivateData(const AbstractOutput& _output):output(_output) {}
		SPrivateData(const SPrivateData& other):output(other.output) {}
	};

	AbstractOutputHandler::AbstractOutputHandler(const AbstractOutput& output)
		: mData(new SPrivateData(output))
	{
	}

	AbstractOutputHandler::AbstractOutputHandler(const AbstractOutputHandler& other)
		: mData(new SPrivateData(*(other.mData)))
	{
	}

	AbstractOutputHandler::AbstractOutputHandler(AbstractOutputHandler&& other)
		: mData(std::move(other.mData))
	{
	}

	AbstractOutputHandler::~AbstractOutputHandler()
	{
		
	}

	AbstractOutputHandler& AbstractOutputHandler::operator=(const AbstractOutputHandler& other) 
	{
		mData.reset(new SPrivateData(*(other.mData)));

		return *this;
	}

	AbstractOutputHandler& AbstractOutputHandler::operator=(AbstractOutputHandler&& other)
	{
		mData = std::move(other.mData);

		return *this;
	}

	void AbstractOutputHandler::setOutput(const AbstractOutput& output)
	{
		mData->output = output;
	}

	AbstractOutput& AbstractOutputHandler::output() 
	{
		return mData->output;
	}

	const AbstractOutput& AbstractOutputHandler::output() const 
	{
		return mData->output;
	}

	const String& AbstractOutputHandler::basePath() const
	{
		return gBASE_PATH;
	}

	String AbstractOutputHandler::createFileName(String fileType, String praefix) const
	{
		String ret = praefix;

		ret += output().timestamp().year();
		ret += output().timestamp().month();
		ret += output().timestamp().day();
		ret += fileType[0] == '.' ? "" : ".";
		ret += fileType;

		return ret;
	}

	String AbstractOutputHandler::createFilePath(String additionalPath) const 
	{
		String ret = basePath();

		ret += additionalPath;
		if(ret[ret.length()-1] != '/') ret += "/";
	
		return ret;
	}
}
