#ifndef __STATEHANDLER_H__
#define __STATEHANDLER_H__

#include <memory>
#include "Settings.h"
#include "structs.h"
#include "Arduino.h"

namespace model
{
	class StateHandler
	{
	public:
		static StateHandler& Instance();
		~StateHandler();
		
		StateHandler(const StateHandler& other) = delete;
		StateHandler(StateHandler&& other) = delete;
		StateHandler& operator=(const StateHandler& other) = delete;
		StateHandler& operator=(StateHandler&& other) = delete;

		MeasurementType getCurrentState();
		bool isShortTerm();
		bool isLongTerm();

	private:
		StateHandler();

		struct SPrivateData;
		std::unique_ptr<SPrivateData> mData;
	};
}

#endif // __STATEHANDLER_H__
