#ifndef __SHORTTERM_PM_OUTPUT_H__
#define __SHORTTERM_PM_OUTPUT_H__

#include "AbstractOutputHandler.h"

namespace model
{
	class ShortTermOutputHandler: public AbstractOutputHandler
	{
	public:
		using AbstractOutputHandler::AbstractOutputHandler;

		virtual void write() const;

	};
}

#endif // __SHORTTERM_PM_OUTPUT_H__
