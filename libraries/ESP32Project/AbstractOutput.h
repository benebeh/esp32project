#ifndef __ABSTRACT_OUTPUT_H__
#define __ABSTRACT_OUTPUT_H__

#include <ArduinoJson.h>
#include "structs.h"
#include "EspRTC.h"


namespace model 
{
    class AbstractOutput
    {
    public:
        virtual operator String() const { return toString(); }
		virtual operator DynamicJsonDocument() const { return toJson(); }

        virtual String toString() const { return ""; }
		virtual DynamicJsonDocument toJson() const 
        { 
            static DynamicJsonDocument ret(24); return ret; 
        }

        void setOutputFormat(OutputType format)
        {
            outputType = format;
        }

		OutputType outputFormat() const
        {
            return outputType;
        }

        virtual bool isFilled() const { return false; }
        virtual const DateTime& timestamp() const
        {
            static DateTime ret;
            return ret;
        }

    private:
        OutputType outputType = CSV;
    };
}



#endif // __ABSTRACT_OUTPUT_H__

