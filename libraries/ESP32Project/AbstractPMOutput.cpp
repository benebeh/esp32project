#include "AbstractPMOutput.h"

namespace model
{
	namespace
	{
		const String gBASE_PATH = "pms/";
	}

	struct AbstractPMOutput::SPrivateData
	{
		PMOutput output;

		SPrivateData(const PMOutput& _output):output(_output) {}
		SPrivateData(const SPrivateData& other):output(other.output) {}
	};

	AbstractPMOutput::AbstractPMOutput(const PMOutput& output)
		: mData(new SPrivateData(output))
	{
	}

	AbstractPMOutput::AbstractPMOutput(const AbstractPMOutput& other)
		: mData(new SPrivateData(*(other.mData)))
	{
	}

	AbstractPMOutput::AbstractPMOutput(AbstractPMOutput&& other)
		: mData(std::move(other.mData))
	{
	}

	AbstractPMOutput::~AbstractPMOutput()
	{
		
	}

	AbstractPMOutput& AbstractPMOutput::operator=(const AbstractPMOutput& other) 
	{
		mData.reset(new SPrivateData(*(other.mData)));

		return *this;
	}

	AbstractPMOutput& AbstractPMOutput::operator=(AbstractPMOutput&& other)
	{
		mData = std::move(other.mData);

		return *this;
	}

	void AbstractPMOutput::setOutput(const PMOutput& output)
	{
		mData->output = output;
	}

	PMOutput& AbstractPMOutput::output() 
	{
		return mData->output;
	}

	const PMOutput& AbstractPMOutput::output() const 
	{
		return mData->output;
	}

	const String& AbstractPMOutput::basePath() const
	{
		return gBASE_PATH;
	}

	String AbstractPMOutput::createFileName(String fileType, String praefix) const
	{
		String ret = praefix;

		ret += output().timestamp().year();
		ret += output().timestamp().month();
		ret += output().timestamp().day();
		ret += fileType[0] == '.' ? "" : ".";
		ret += fileType;

		return ret;
	}

	String AbstractPMOutput::createFilePath(String additionalPath) const 
	{
		String ret = basePath();

		ret += additionalPath;
		if(ret[ret.length()-1] != '/') ret += "/";
	
		return ret;
	}
}
