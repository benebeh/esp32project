#ifndef __SPS30_HANDLER_H__
#define __SPS30_HANDLER_H__

#include "structs.h"
#include <memory>

#include "PMOutput.h"
#include "EspRTC.h"

#include "Arduino.h"
#include "Wire.h"
#include "Settings.h"

#include "sensirion_uart.h"          //change sensirion_uart.cpp RX Pin36 / TX Pin4
#include "sps30.h"

namespace model
{
    class Sps30Handler
    {
    public:
        static Sps30Handler& Instance();

        ~Sps30Handler();
        Sps30Handler(const Sps30Handler& other) = delete;
		Sps30Handler(Sps30Handler&& other) = delete;
		Sps30Handler& operator=(const Sps30Handler& other) = delete;
		Sps30Handler& operator=(Sps30Handler&& other) = delete;

        PMOutput read();

    private:
        Sps30Handler();

        PMOutput readI2C();
        PMOutput readUART();

        void setPointer(byte p1, byte p2) const;
        bool calculateCRC(byte data[2]) const;

        void startMeasurement() const;
        void startFanCleaning() const;
        void assignValueToStruct(unsigned currentIndex, float value, SMassConcentrationData& mass, SQuantityConcentrationData& quantity) const;

        void sps30ErrorToMessage(char *mess, uint8_t r) const;

        struct SPrivateData;
        std::unique_ptr<SPrivateData> mData;

    };
}

#endif // __SPS30_HANDLER_H__
