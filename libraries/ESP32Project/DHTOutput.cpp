#include "DHTOutput.h"

namespace model
{
    struct DHTOutput::SPrivateData
    {
        DateTime timestamp;
        SDHTData data;
        bool isFilled = false;

        SPrivateData() {}
        SPrivateData(const SPrivateData& other): timestamp(other.timestamp), data(other.data), isFilled(other.isFilled) {}
    };

    DHTOutput::DHTOutput()
        : mData(new SPrivateData())
    {
    }

    DHTOutput::DHTOutput(DHTOutput&& other)
        : mData(std::move(other.mData))
    {
    }

    DHTOutput::DHTOutput(const DHTOutput& other)
        : mData(new SPrivateData(* (other.mData)))
    {
    }

    DHTOutput::~DHTOutput()
    {
    }

    DHTOutput & DHTOutput::operator=(const DHTOutput& other)
    {
        mData.reset(new SPrivateData(*(other.mData)));

        return *this;
    }

    DHTOutput & DHTOutput::operator=(DHTOutput&& other)
    {
        mData = std::move(other.mData);

        return *this;
    }

    String DHTOutput::toString() const
    {
        String ret;
        if(outputFormat() == JSON)
        {
             serializeJson(toJson(), ret);
        }
        else 
        {
            std::vector<String> output = { String("dht"), EspRTC::dateTimeToString(timestamp()), String(data().humidity), 
                                           String(data().temperature) };

            for(const auto& str: output)
            {
                ret += ret.length() > 0 ? ";" :"";
                ret += str;
            }
        }

        return ret;
    }

    DynamicJsonDocument DHTOutput::toJson() const
    {
        DynamicJsonDocument ret(1024);
        JsonObject  root = ret.to<JsonObject>();
        JsonObject  dataObj = ret.createNestedObject("data");

        String date = EspRTC::dateTimeToString(timestamp());

        root["timestamp"] = date;
        root["type"] = "dht";
        dataObj["humidity"] = data().humidity;
        dataObj["temperature"] = data().temperature;

        return ret;
    }

    void DHTOutput::setTimestamp(const DateTime& timestamp)
    {
        mData->timestamp = timestamp;
    }

    const DateTime& DHTOutput::timestamp() const
    {
        return mData->timestamp;
    }

    void DHTOutput::setData(const SDHTData& data)
    {
        mData->data = data;
        mData->isFilled = true;
    }

    const SDHTData& DHTOutput::data() const
    {
        return mData->data;
    }

    bool DHTOutput::isFilled() const
    {
        return mData->isFilled;
    }
}
