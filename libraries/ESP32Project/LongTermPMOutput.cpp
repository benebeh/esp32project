#include "LongTermPMOutput.h"
#include "SDHandle.h"

namespace model
{
	void LongTermPMOutput::write() const
	{
		SDHandle::Instance().saveData(output().toString(), createFilePath("longTerm") + createFileName("csv", "pmData_"), true);
	}
}
