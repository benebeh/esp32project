#ifndef __SHORTTERM_PM_OUTPUT_H__
#define __SHORTTERM_PM_OUTPUT_H__

#include "AbstractPMOutput.h"

namespace model
{
	class ShortTermPMOutput: public AbstractPMOutput
	{
	public:
		using AbstractPMOutput::AbstractPMOutput;

		virtual void write() const;

	};
}

#endif // __SHORTTERM_PM_OUTPUT_H__
