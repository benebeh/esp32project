#ifndef __LONGTERM_PM_OUTPUT_H__
#define __LONGTERM_PM_OUTPUT_H__

#include "AbstractOutputHandler.h"

namespace model
{
	class LongTermOutputHandler : public AbstractOutputHandler
	{
	public:
		using AbstractOutputHandler::AbstractOutputHandler;

		virtual void write() const;

	};
}

#endif // __LONGTERM_PM_OUTPUT_H__
