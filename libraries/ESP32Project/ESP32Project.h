#ifndef __ESP32_MODEL_H__
#define __ESP32_MODEL_H__

#include "Arduino.h"

#include "EspRTC.h"
#include "DHTHandler.h"
#include "LongTermOutputHandler.h"
#include "PMOutput.h"
#include "SDHandle.h"
#include "ShortTermOutputHandler.h"
#include "Sps30Handler.h"
#include "StateHandler.h"
#include "Settings.h"
#include "structs.h"

#endif // __ESP32_MODEL_H__
