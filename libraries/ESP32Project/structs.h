#ifndef __ESP32PROJECT_MODEL_STRsUCTS_H__
#define __ESP32PROJECT_MODEL_STRsUCTS_H__

namespace model
{
    struct SMassConcentrationData
	{
		double pm1 = 0.;
		double pm2d5 = 0.;
		double pm4 = 0.;
		double pm10 = 0.;
	};

	struct SQuantityConcentrationData
	{
		double pm0d5 = 0.;
		double pm1 = 0.;
		double pm2d5 = 0.;
		double pm4 = 0.;
		double pm10 = 0.;
	};

	struct SDHTData
	{
		float humidity;
		float temperature;
	};

	enum ProtocolType
	{
		UART,
		I2C 
	};

	enum OutputType 
	{
		JSON, 
		CSV
	};

	enum MeasurementType
	{
		ShortTerm,
		LongTerm
	};	
}

#endif // __ESP32PROJECT_MODEL_STRUCTS_H__
