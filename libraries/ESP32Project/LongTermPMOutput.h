#ifndef __LONGTERM_PM_OUTPUT_H__
#define __LONGTERM_PM_OUTPUT_H__

#include "AbstractPMOutput.h"

namespace model
{
	class LongTermPMOutput : public AbstractPMOutput
	{
	public:
		using AbstractPMOutput::AbstractPMOutput;

		virtual void write() const;

	};
}

#endif // __LONGTERM_PM_OUTPUT_H__
