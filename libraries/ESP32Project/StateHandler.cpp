#include "StateHandler.h"

namespace model 
{
    namespace 
    {
        unsigned gDEBOUNCE_DELAY = 50;
    }

    struct StateHandler::SPrivateData 
    {
        int lastButtonState;
    };

    StateHandler::StateHandler()
         : mData(new SPrivateData()) 
    {
        pinMode(Settings::Instance().stateHandlerButtonPin(), INPUT);
    }

    StateHandler::~StateHandler() 
    {
    }

    StateHandler& StateHandler::Instance() 
    {
        static StateHandler ret;

        return ret;
    }

    MeasurementType StateHandler::getCurrentState()
    {
        if(!Settings::Instance().useStateHandler())
        {
            return Settings::Instance().defaultMeasurementType();
        }

        unsigned buttonPin = Settings::Instance().stateHandlerButtonPin();
        int reading = digitalRead(buttonPin);

        if(reading != mData->lastButtonState)
        {
            delay(gDEBOUNCE_DELAY);
        }

        reading = digitalRead(buttonPin);
        
        if(reading == HIGH) 
        {
            return Settings::Instance().defaultMeasurementType();
        } 
        else
        {
            return Settings::Instance().defaultMeasurementType() == LongTerm ? ShortTerm : LongTerm;
        } 
    }

    bool StateHandler::isShortTerm() 
    {
        return getCurrentState() == ShortTerm;
    }

    bool StateHandler::isLongTerm() 
    {
        return getCurrentState() == LongTerm;
    }
}
