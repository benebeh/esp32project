#ifndef __PMOUTPUT_H__
#define __PMOUTPUT_H__

#include <string>
#include <memory>

#include <ArduinoJson.h>
#include "EspRTC.h"
#include "structs.h"

#include "AbstractOutput.h"

namespace model
{
	class PMOutput: public AbstractOutput
	{
	public:
		PMOutput();
		PMOutput(const PMOutput& other);
		PMOutput(PMOutput&& other);
		~PMOutput();

		PMOutput& operator=(const PMOutput& other);
		PMOutput& operator=(PMOutput&& other);

		void setTimestamp(const DateTime& timestamp);
		virtual const DateTime& timestamp() const;

		void setMassConcentrationData(const SMassConcentrationData& data);
		const SMassConcentrationData& massConcentrationData() const;

		void setQuantityConcentrationData(const SQuantityConcentrationData& data);
		const SQuantityConcentrationData& quantityConcentrationData() const;

		virtual bool isFilled() const;
	
		virtual String toString() const;
		virtual DynamicJsonDocument toJson() const;

	private:
		struct SPrivateData;
		std::unique_ptr<SPrivateData> mData;

	};
}

#endif // __PMOUTPUT_H__
