#ifndef __ABSTRACT_PM_OUTPUT_H__
#define __ABSTRACT_PM_OUTPUT_H__

#include <string>
#include <memory>
#include "PMOutput.h"

namespace model
{
	class AbstractPMOutput
	{
	public:
		AbstractPMOutput(const PMOutput& out);
		AbstractPMOutput();
		~AbstractPMOutput();

		AbstractPMOutput(const AbstractPMOutput& other);
		AbstractPMOutput(AbstractPMOutput&& other);
		AbstractPMOutput& operator=(const AbstractPMOutput& other);
		AbstractPMOutput& operator=(AbstractPMOutput&& other);

		void setOutput(const PMOutput& out);
		const PMOutput& output() const;
		PMOutput& output();

		virtual void write() const = 0;

	protected:
		const String& basePath() const;
		String createFileName(String fileType, String praefix = "") const;
		String createFilePath(String additionalPath = "") const;

	private:
		struct SPrivateData;
		std::unique_ptr<SPrivateData> mData;

	};
}

#endif // __ABSTRACT_PM_OUTPUT_H__
