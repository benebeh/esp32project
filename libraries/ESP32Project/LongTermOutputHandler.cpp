#include "LongTermOutputHandler.h"
#include "SDHandle.h"

namespace model
{
	void LongTermOutputHandler::write() const
	{
		SDHandle::Instance().saveData((String) output(), createFilePath("longTerm") + createFileName("csv", "longtermData_"), true);
	}
}
