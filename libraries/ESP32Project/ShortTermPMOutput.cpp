#include "ShortTermPMOutput.h"
#include "SDHandle.h"

namespace model
{
	void ShortTermPMOutput::write() const
	{
		SDHandle::Instance().saveData(output().toString(), createFilePath("shortTerm") + createFileName("csv", "pmData_"), false);
	}
}

