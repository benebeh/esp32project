#include "Settings.h"

// wird zur Laufzeit gesetzt

#ifndef LONG_TERM_MEASURE_DELAY
#define LONG_TERM_MEASURE_DELAY LONG_TERM_MEASURE_DELAY_DEFAULT
#endif

#ifndef SHORT_TERM_MEASURE_DELAY
#define SHORT_TERM_MEASURE_DELAY SHORT_TERM_MEASURE_DELAY_DEFAULT
#endif 

#ifndef SPS30_ADDRESS
#define SPS30_ADDRESS SPS30_ADDRESS_DEFAULT
#endif 

#ifndef SPS30_PROTOCOL_USE_I2C
#define SPS30_PROTOCOL_USE_I2C SPS30_PROTOCOL_USE_I2C_DEFAULT
#endif 

#ifndef STATE_HANDLER_BUTTON_PIN
#define STATE_HANDLER_BUTTON_PIN STATE_HANDLER_BUTTON_PIN_DEFAULT
#endif

#ifndef DEFAULT_MEASUREMENT_TYPE_IS_LONGTERM
#define DEFAULT_MEASUREMENT_TYPE_IS_LONGTERM DEFAULT_MEASUREMENT_TYPE_IS_LONGTERM_DEFAULT
#endif 

#ifndef USE_STATE_HANDLER_BUTTON 
#define USE_STATE_HANDLER_BUTTON USE_STATE_HANDLER_BUTTON_DEFAULT
#endif 

#ifndef TICK_DELAY
#define TICK_DELAY TICK_DELAY_DEFAULT
#endif 

namespace model 
{
    namespace 
    {
        SettingsInput gSETTINGS_INPUT;
    }

    struct Settings::SPrivateData 
    {
        SettingsInput* settings;

        SPrivateData(SettingsInput* _settings):settings(_settings) {};
    };

    Settings::Settings(SettingsInput* settings)
        : mData(new SPrivateData(settings))
    {
    }

    Settings::~Settings()
    {
    }

    const Settings& Settings::Instance()
    {
        static Settings ret(&gSETTINGS_INPUT);
        
        return ret;
    }

    void Settings::changeSettings(const SettingsInput& input) 
    {
        gSETTINGS_INPUT = input;
    }

    unsigned Settings::longTermMeasureDelay() const 
    {
        return mData->settings->longTermMeasureDelay;
    }

    unsigned Settings::shortTermMeasureDelay() const 
    {
        return mData->settings->shortTermMeasureDelay;
    }

    uint8_t Settings::sps30Address() const 
    {
        return mData->settings->sps30Address;
    }

    ProtocolType Settings::sps30ProtocolType() const 
    {
        return mData->settings->sps30ProtocolType;
    }

    unsigned Settings::stateHandlerButtonPin() const 
    {
        return mData->settings->stateHandlerButtonPin;
    }

    MeasurementType Settings::defaultMeasurementType() const 
    {
        return mData->settings->defaultMeasurementType;
    }

    bool Settings::useStateHandler() const 
    {
        return mData->settings->useStateHandler;
    }

    unsigned Settings::tickDelay() const 
    {
        return mData->settings->tickDelay;
    }

    unsigned Settings::dhtPin() const 
    {
        return mData->settings->dhtPin;
    }

    unsigned Settings::sps30UARTRxPin() const
    {
        return mData->settings->sps30UARTRx;
    }
    
    unsigned Settings::sps30UARTTxPin() const
    {
        return mData->settings->sps30UARTTx;
    }
}
