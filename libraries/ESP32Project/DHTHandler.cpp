#include "DHTHandler.h"

namespace model 
{
    struct DHTHandler::SPrivateData 
    {
        DHT dht;

        SPrivateData(unsigned dhtPin): dht(dhtPin, DHT22) {}
    };

    DHTHandler::DHTHandler()
        : mData(new SPrivateData(Settings::Instance().dhtPin()))
    {
        pinMode(Settings::Instance().dhtPin(), INPUT);
    }

    DHTHandler::~DHTHandler()
    {
    }

    DHTHandler& DHTHandler::Instance()
    {
        static DHTHandler ret;
        
        return ret;
    }

    DHTOutput DHTHandler::read()
    {
        Serial.println("Starte DHT-Sensor!");
        mData->dht.begin();

        Serial.println("DHT: Begin read");
        float humidity    = mData->dht.readHumidity();
        float temperature = mData->dht.readTemperature();

        DHTOutput ret;

        if(isnan(humidity) || isnan(temperature))
        {
            Serial.println("DHT: Konnte nicht vom DHT ablesen!");
            return ret;
        }
        else 
        {
            Serial.println("DHT: Habe folgendes vom DHT gelesen: ");
            Serial.print("DHT: Temperatur: ");
            Serial.println(temperature);
            Serial.print("DHT: Luftfeuchtigkeit: ");
            Serial.println(humidity);
        }

        SDHTData data;
        data.humidity = humidity;
        data.temperature = temperature;

        Serial.println("DHT: Speichern");
        ret.setTimestamp(EspRTC::Instance().getCurrentTime());
        ret.setData(data);
        Serial.println("DHT: Fertig!");

        return ret;
    }
}
