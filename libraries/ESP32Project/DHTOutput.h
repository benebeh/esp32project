#ifndef __DHT_OUTPUT_H__
#define __DHT_OUTPUT_H__

#include "AbstractOutput.h"
#include "EspRTC.h"
#include "structs.h"

namespace model
{
    class DHTOutput: public AbstractOutput
    {
    public:
        DHTOutput();
        DHTOutput(DHTOutput&& other);
        DHTOutput(const DHTOutput& other);
        ~DHTOutput();

        DHTOutput & operator=(const DHTOutput& other);
        DHTOutput & operator=(DHTOutput&& other);

        virtual String toString() const;
		virtual DynamicJsonDocument toJson() const;

        void setTimestamp(const DateTime& timestamp);
		virtual const DateTime& timestamp() const;
    
        void setData(const SDHTData& data);
        const SDHTData& data() const;

        virtual bool isFilled() const;

    private:
        struct SPrivateData;
        std::unique_ptr<SPrivateData> mData;
    };
}

#endif // __DHT_OUTPUT_H__
