#include "Sps30Handler.h"

#define SP30_COMMS SERIALPORT1

namespace model 
{
    struct Sps30Handler::SPrivateData
    {
//        SPS30 uartSPS;
        bool initializedUART = false;
    };

    Sps30Handler& Sps30Handler::Instance()
    {
        static Sps30Handler ret;

        return ret;
    }

    Sps30Handler::Sps30Handler()
        :  mData(new SPrivateData())
    {
        const Settings& settings(Settings::Instance());

        if(settings.sps30ProtocolType() == I2C) 
        {
            Wire.begin();
            delay(100);

            startMeasurement();
            startFanCleaning();
        }
        else 
        {
            /*
            mData->uartSPS.SetSerialPin(settings.sps30UARTRxPin(), settings.sps30UARTTxPin());
            if(!mData->uartSPS.begin(SP30_COMMS))
            {
                Serial.println("Konnte sps30 nicht ueber UART initialisieren!");
                return;
            }

            if(!mData->uartSPS.probe())
            {
                Serial.println("Konnte mich nicht mit dem SPS30 ueber UART verbinden!");
                return;
            }
            Serial.println("sps30 gefunden (UART)");

            if(!mData->uartSPS.reset())
            {
                Serial.println("Konnte sps30 nicht ueber UART resetten!");
                return;
            }
            */       

            mData->initializedUART = true;
        }
    }

    Sps30Handler::~Sps30Handler()
    {
    }

    PMOutput Sps30Handler::read()
    {
        return Settings::Instance().sps30ProtocolType() == I2C ? readI2C() : readUART();
    }

    PMOutput Sps30Handler::readUART()
    {

        PMOutput ret;
        if(!mData->initializedUART) 
        {
            Serial.println("sps30 nicht initialisiert, breche Messung ab!");
            return ret;
        }

        sensirion_uart_open();

        while (sps30_probe() != 0) {
            Serial.println("sps30 probe failed - wait 1000ms");
            delay(1000);
        }
        // sps30_set_fan_auto_cleaning_interval(60*60);

        /* start measurement and wait for 10s to ensure the sensor has a
        * stable flow and possible remaining particles are cleaned out */
        if (sps30_start_measurement() != 0) {
            Serial.println("error starting measurement");
            return ret;
        }

        struct sps30_measurement measurement;
        s16 err;

        Serial.println("Starte Sps30-Measurement");
        while(true) 
        {
            delay(2000);
            err = sps30_read_measurement(&measurement);

            if(err < 0)
            {
                Serial.println("Konnte nicht von sps30 lesen, versuche noch mal");
            }
            else 
            {
                if(SPS_IS_ERR_STATE(err))
                {
                    Serial.println("Problem beim lesen von SPS30, versuche noch mal");
                }
                else 
                {
                    break;
                }
            }
        }

        SMassConcentrationData mass;
        SQuantityConcentrationData quantity;

        mass.pm1 = measurement.mc_1p0;
        mass.pm2d5 = measurement.mc_2p5;
        mass.pm4 = measurement.mc_4p0;
        mass.pm10 = measurement.mc_10p0;

        quantity.pm0d5 = measurement.nc_0p5;
        quantity.pm1 = measurement.nc_1p0;
        quantity.pm2d5 = measurement.nc_2p5;
        quantity.pm4 = measurement.nc_4p0;
        quantity.pm10 = measurement.nc_10p0;

        ret.setMassConcentrationData(mass);
        ret.setQuantityConcentrationData(quantity);
        ret.setTimestamp(EspRTC::Instance().getCurrentTime());

        sps30_stop_measurement();
        sensirion_uart_close();

        return ret;
    }

/*
    PMOutput Sps30Handler::readUART()
    {
        PMOutput ret;
        if(!mData->initializedUART) 
        {
            Serial.println("sps30 nicht initialisiert, breche Messung ab!");
            return ret;
        }

        if(!mData->uartSPS.start())
        {
            Serial.println("Konnte Messung nicht starten, breche ab!");
            return ret;
        }

        struct sps_values val;
        uint8_t measurementResult, error_cnt = 0;

        do {
            measurementResult = mData->uartSPS.GetValues(&val);

            if(measurementResult == ERR_DATALENGTH) 
            {
                if(error_cnt++ > 3) 
                {
                    sps30ErrorToMessage("Fehler bei der Messung!", measurementResult);
                    return ret;
                }

                delay(1000);
            }
            else if(measurementResult != ERR_OK)
            {
                sps30ErrorToMessage("Fehler bei der Messung!", measurementResult);
                return ret;
            }
        } while(measurementResult != ERR_OK);

        SMassConcentrationData mass;
        SQuantityConcentrationData quantity;

        mass.pm1 = val.MassPM1;
        mass.pm2d5 = val.MassPM2;
        mass.pm4 = val.MassPM4;
        mass.pm10 = val.MassPM10;

        quantity.pm0d5 = val.NumPM0;
        quantity.pm1 = val.NumPM1;
        quantity.pm2d5 = val.NumPM2;
        quantity.pm4 = val.NumPM4;
        quantity.pm10 = val.NumPM10;

        ret.setMassConcentrationData(mass);
        ret.setQuantityConcentrationData(quantity);
        ret.setTimestamp(EspRTC::Instance().getCurrentTime());
    }
*/

    PMOutput Sps30Handler::readI2C()
    {
        static  PMOutput ret;
        return ret;
        /*
        struct sps_values val;
        PMOutput ret;
        SMassConcentrationData mass;
        SQuantityConcentrationData quantity;
        DateTime timestamp;
        unsigned address = Settings::Instance().sps30Address();      // Zur Laufzeit holen, falls sich was an den defines Ã¤ndert.
        byte nd[60];
        float measure;
        long tmp;
        byte w1, w2, w3;

        Serial.println("sps30: Start reading from Device.");
        setPointer(0x02, 0x02);
        Wire.requestFrom(address, 3);
        w1 = Wire.read();
        w2 = Wire.read();
        w3 = Wire.read();

        unsigned currentField = 0;

        if(w2 == 0x01) 
        {
            Serial.println("sps30: Data is ready to load.");
            setPointer(0x03, 0x00);
            Wire.requestFrom(address, 60);
            for (int i = 0; i < 60; i++)
            {
                nd[i] = Wire.read();
            }
            // Result: PM1.0/PM2.5/PM4.0,PM10 , PM0.5,PM1.0/PM2.5/PM4.0,PM10
            Serial.print("sps30: ");
            for (int i = 0; i < 60; i++)
            {
                if ((i + 1) % 3 == 0)
                {
                    byte datax[2] = { nd[i - 2], nd[i - 1] };

                    if (tmp == 0)
                    {
                        tmp = nd[i - 2];
                        tmp = (tmp << 8) + nd[i - 1];
                    }
                    else
                    {
                        tmp = (tmp << 8) + nd[i - 2];
                        tmp = (tmp << 8) + nd[i - 1];
                        measure = (*(float*)&tmp);
                        
                        Serial.print(measure);
                        Serial.print(" ");

                        assignValueToStruct(currentField++, measure, mass, quantity);

                        tmp = 0;
                    }
                }
            }
        }
        else 
        {
            Serial.println("sps30: Data is not ready yet to be loaded. Returning empty object.");
            return ret;
        }

        Serial.println("sps30: Finished Loading data!");
        
        timestamp = EspRTC::Instance().getCurrentTime();

        ret.setMassConcentrationData(mass);
        ret.setQuantityConcentrationData(quantity);
        ret.setTimestamp(timestamp);

        return ret;
        */
    }

    void Sps30Handler::setPointer(byte p1, byte p2) const
    {
        Wire.beginTransmission(Settings::Instance().sps30Address());
        Wire.write(p1);
        Wire.write(p2);
        Wire.endTransmission();
    }

    bool Sps30Handler::calculateCRC(byte data[2]) const 
    {
        byte crc = 0xFF;
        for(int i = 0; i < 2; i++) {
            crc ^= data[i];

            for(byte bit = 8; bit > 0; --bit) {
                if(crc & 0x80) {
                    crc = (crc << 1) ^ 0x31u;
                } else {
                    crc = (crc << 1);
                }
            }
        }

        return crc;
    }

    void Sps30Handler::startMeasurement() const 
    {
        Wire.beginTransmission(Settings::Instance().sps30Address());
        Wire.write(0x00);
        Wire.write(0x10);
        Wire.write(0x03);
        Wire.write(0x00);
        uint8_t data[2]={0x03, 0x00};
        Wire.write(calculateCRC(data));
        Wire.endTransmission();

        Serial.println("sps30: Started Measurement. Waiting for 10s...");
        delay(10000);
        Serial.println("sps30: Measurement init finished!");
    }

    void Sps30Handler::startFanCleaning() const 
    {
        setPointer(0x56, 0x07);
        
        Serial.println("sps30: Started fan cleaning. Waiting for 12s...");
        delay(12000);
        Serial.println("sps30: Clean finished!");
    }

    void Sps30Handler::assignValueToStruct(unsigned currentIndex, float value, SMassConcentrationData& mass, SQuantityConcentrationData& quantity) const
    {
        double conv = static_cast<double>(value);

        switch(currentIndex)
        {
            case 0:
                mass.pm1 = conv;
                break;
            case 1:
                mass.pm2d5 = conv;
                break; 
            case 2:
                mass.pm4 = conv;
                break; 
            case 3:
                mass.pm10 = conv;
                break;
            case 4:
                quantity.pm0d5 = conv;
                break; 
            case 5: 
                quantity.pm1 = conv; 
                break;
            case 6: 
                quantity.pm2d5 = conv; 
                break;
            case 7: 
                quantity.pm4 = conv; 
                break;
            case 8: 
                quantity.pm10 = conv; 
                break;
            /// @TODO : danach?
        }
    }

    void Sps30Handler::sps30ErrorToMessage(char *mess, uint8_t r) const
    {
        char buf[80];

        Serial.print(mess);

        // mData->uartSPS.GetErrDescription(r, buf, 80);
        Serial.println(buf);
    }
}
