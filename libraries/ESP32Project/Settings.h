#ifndef __ESP32_SETTINGS_H__
#define __ESP32_SETTINGS_H__
#include "structs.h"
#include <memory>

#define LONG_TERM_MEASURE_DELAY_DEFAULT 1000*60*10
#define SHORT_TERM_MEASURE_DELAY_DEFAULT 1000*10
#define SPS30_ADDRESS_DEFAULT 0x69
#define SPS30_PROTOCOL_USE_I2C_DEFAULT true 
#define STATE_HANDLER_BUTTON_PIN_DEFAULT 2
#define DEFAULT_MEASUREMENT_TYPE_IS_LONGTERM_DEFAULT true 
#define USE_STATE_HANDLER_BUTTON_DEFAULT false
#define TICK_DELAY_DEFAULT 1000
#define DHT_PIN_DEFAULT 32
#define SPS30_UART_RX_DEFAULT 36
#define SPS30_UART_TX_DEFAULT 4

namespace model
{
    struct SettingsInput 
    {
        unsigned longTermMeasureDelay = LONG_TERM_MEASURE_DELAY_DEFAULT;
        unsigned shortTermMeasureDelay = SHORT_TERM_MEASURE_DELAY_DEFAULT;
        uint8_t sps30Address = SPS30_ADDRESS_DEFAULT;
        unsigned stateHandlerButtonPin = STATE_HANDLER_BUTTON_PIN_DEFAULT;
        ProtocolType sps30ProtocolType = SPS30_PROTOCOL_USE_I2C_DEFAULT ? I2C : UART;
        MeasurementType defaultMeasurementType = DEFAULT_MEASUREMENT_TYPE_IS_LONGTERM_DEFAULT ? LongTerm : ShortTerm;
        bool useStateHandler = USE_STATE_HANDLER_BUTTON_DEFAULT;
        unsigned tickDelay = TICK_DELAY_DEFAULT;
        unsigned dhtPin = DHT_PIN_DEFAULT; 
        unsigned sps30UARTRx = SPS30_UART_RX_DEFAULT;
        unsigned sps30UARTTx = SPS30_UART_TX_DEFAULT;
    };

    class Settings 
    {
    public: 
        static const Settings& Instance();
        static void changeSettings(const SettingsInput& input);
        ~Settings();

        unsigned longTermMeasureDelay() const;
        unsigned shortTermMeasureDelay() const;

        uint8_t sps30Address() const;
        ProtocolType sps30ProtocolType() const;
        unsigned stateHandlerButtonPin() const;
        MeasurementType defaultMeasurementType() const;
        bool useStateHandler() const;
        unsigned tickDelay() const;
        unsigned dhtPin() const;
        unsigned sps30UARTRxPin() const;
        unsigned sps30UARTTxPin() const;

    private:
        Settings(SettingsInput* settings);

        struct SPrivateData;
        std::unique_ptr<SPrivateData> mData;
    };
}

#endif // __ESP32_SETTINGS_H__
