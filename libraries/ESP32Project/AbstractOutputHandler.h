#ifndef __ABSTRACT_PM_OUTPUT_H__
#define __ABSTRACT_PM_OUTPUT_H__

#include <string>
#include <memory>
#include "AbstractOutput.h"

namespace model
{
	class AbstractOutputHandler
	{
	public:
		AbstractOutputHandler(const AbstractOutput& out);
		AbstractOutputHandler();
		~AbstractOutputHandler();

		AbstractOutputHandler(const AbstractOutputHandler& other);
		AbstractOutputHandler(AbstractOutputHandler&& other);
		AbstractOutputHandler& operator=(const AbstractOutputHandler& other);
		AbstractOutputHandler& operator=(AbstractOutputHandler&& other);

		void setOutput(const AbstractOutput& out);
		const AbstractOutput& output() const;
		AbstractOutput& output();

		virtual void write() const = 0;

	protected:
		const String& basePath() const;
		String createFileName(String fileType, String praefix = "") const;
		String createFilePath(String additionalPath = "") const;

	private:
		struct SPrivateData;
		std::unique_ptr<SPrivateData> mData;

	};
}

#endif // __ABSTRACT_PM_OUTPUT_H__
