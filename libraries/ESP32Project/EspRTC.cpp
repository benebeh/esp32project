#include "EspRTC.h"
#include "Arduino.h"

#ifdef __AVR__
 #include <avr/pgmspace.h>
#elif defined(ESP8266)
 #include <pgmspace.h>
#elif defined(ARDUINO_ARCH_SAMD)
// nothing special needed
#elif defined(ARDUINO_SAM_DUE)
 #define PROGMEM
 #define pgm_read_byte(addr) (*(const unsigned char *)(addr))
 #define Wire Wire1
#endif

namespace model
{
    namespace 
    {
        String fillWithZeros(const String& input, uint8_t count)
        {
            String ret = input;
            while(ret.length() < count)
            {
                ret = String("0" + ret);
            }

            return ret;
        }

        String fillWithZeros(String&& input, uint8_t count)
        {
            String ret = input;

            while(ret.length() < count)
            {
                ret = String("0" + ret);
            }

            return ret;
        }

        const uint8_t daysInMonth [] PROGMEM = { 31,28,31,30,31,30,31,31,30,31,30,31 };

        // number of days since 2000/01/01, valid for 2001..2099
        static uint16_t date2days(uint16_t y, uint8_t m, uint8_t d) {
            if (y >= 2000)
                y -= 2000;
            uint16_t days = d;
            for (uint8_t i = 1; i < m; ++i)
                days += pgm_read_byte(daysInMonth + i - 1);
            if (m > 2 && y % 4 == 0)
                ++days;
            return days + 365 * y + (y + 3) / 4 - 1;
        }

        static long time2long(uint16_t days, uint8_t h, uint8_t m, uint8_t s) {
            return ((days * 24L + h) * 60 + m) * 60 + s;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // DateTime implementation - ignores time zones and DST changes
    // NOTE: also ignores leap seconds, see http://en.wikipedia.org/wiki/Leap_second

    DateTime::DateTime (uint32_t t) {
        t -= SECONDS_FROM_1970_TO_2000;    // bring to 2000 timestamp from 1970

        ss = t % 60;
        t /= 60;
        mm = t % 60;
        t /= 60;
        hh = t % 24;
        uint16_t days = t / 24;
        uint8_t leap;
        for (yOff = 0; ; ++yOff) {
            leap = yOff % 4 == 0;
            if (days < 365 + leap)
                break;
            days -= 365 + leap;
        }
        for (m = 1; ; ++m) {
            uint8_t daysPerMonth = pgm_read_byte(daysInMonth + m - 1);
            if (leap && m == 2)
                ++daysPerMonth;
            if (days < daysPerMonth)
                break;
            days -= daysPerMonth;
        }
        d = days + 1;
    }

    DateTime::DateTime (uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec) {
        if (year >= 2000)
            year -= 2000;
        yOff = year;
        m = month;
        d = day;
        hh = hour;
        mm = min;
        ss = sec;
    }

    DateTime::DateTime (const DateTime& copy):
    yOff(copy.yOff),
    m(copy.m),
    d(copy.d),
    hh(copy.hh),
    mm(copy.mm),
    ss(copy.ss)
    {}

    static uint8_t conv2d(const char* p) {
        uint8_t v = 0;
        if ('0' <= *p && *p <= '9')
            v = *p - '0';
        return 10 * v + *++p - '0';
    }

    // A convenient constructor for using "the compiler's time":
    //   DateTime now (__DATE__, __TIME__);
    // NOTE: using F() would further reduce the RAM footprint, see below.
    DateTime::DateTime (const char* date, const char* time) {
        // sample input: date = "Dec 26 2009", time = "12:34:56"
        yOff = conv2d(date + 9);
        // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec 
        switch (date[0]) {
            case 'J': m = (date[1] == 'a') ? 1 : ((date[2] == 'n') ? 6 : 7); break;
            case 'F': m = 2; break;
            case 'A': m = date[2] == 'r' ? 4 : 8; break;
            case 'M': m = date[2] == 'r' ? 3 : 5; break;
            case 'S': m = 9; break;
            case 'O': m = 10; break;
            case 'N': m = 11; break;
            case 'D': m = 12; break;
        }
        d = conv2d(date + 4);
        hh = conv2d(time);
        mm = conv2d(time + 3);
        ss = conv2d(time + 6);
    }

    // A convenient constructor for using "the compiler's time":
    // This version will save RAM by using PROGMEM to store it by using the F macro.
    //   DateTime now (F(__DATE__), F(__TIME__));
    DateTime::DateTime (const __FlashStringHelper* date, const __FlashStringHelper* time) {
        // sample input: date = "Dec 26 2009", time = "12:34:56"
        char buff[11];
        memcpy_P(buff, date, 11);
        yOff = conv2d(buff + 9);
        // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
        switch (buff[0]) {
            case 'J': m = (buff[1] == 'a') ? 1 : ((buff[2] == 'n') ? 6 : 7); break;
            case 'F': m = 2; break;
            case 'A': m = buff[2] == 'r' ? 4 : 8; break;
            case 'M': m = buff[2] == 'r' ? 3 : 5; break;
            case 'S': m = 9; break;
            case 'O': m = 10; break;
            case 'N': m = 11; break;
            case 'D': m = 12; break;
        }
        d = conv2d(buff + 4);
        memcpy_P(buff, time, 8);
        hh = conv2d(buff);
        mm = conv2d(buff + 3);
        ss = conv2d(buff + 6);
    }

    uint8_t DateTime::dayOfTheWeek() const {    
        uint16_t day = date2days(yOff, m, d);
        return (day + 6) % 7; // Jan 1, 2000 is a Saturday, i.e. returns 6
    }

    uint32_t DateTime::unixtime(void) const {
    uint32_t t;
    uint16_t days = date2days(yOff, m, d);
    t = time2long(days, hh, mm, ss);
    t += SECONDS_FROM_1970_TO_2000;  // seconds from 1970 to 2000

    return t;
    }

    long DateTime::secondstime(void) const {
    long t;
    uint16_t days = date2days(yOff, m, d);
    t = time2long(days, hh, mm, ss);
    return t;
    }

    DateTime DateTime::operator+(const TimeSpan& span) {
    return DateTime(unixtime()+span.totalseconds());
    }

    DateTime DateTime::operator-(const TimeSpan& span) {
    return DateTime(unixtime()-span.totalseconds());
    }

    TimeSpan DateTime::operator-(const DateTime& right) {
    return TimeSpan(unixtime()-right.unixtime());
    }

    ////////////////////////////////////////////////////////////////////////////////
    // TimeSpan implementation

    TimeSpan::TimeSpan (int32_t seconds):
    _seconds(seconds)
    {}

    TimeSpan::TimeSpan (int16_t days, int8_t hours, int8_t minutes, int8_t seconds):
    _seconds((int32_t)days*86400L + (int32_t)hours*3600 + (int32_t)minutes*60 + seconds)
    {}

    TimeSpan::TimeSpan (const TimeSpan& copy):
    _seconds(copy._seconds)
    {}

    TimeSpan TimeSpan::operator+(const TimeSpan& right) {
    return TimeSpan(_seconds+right._seconds);
    }

    TimeSpan TimeSpan::operator-(const TimeSpan& right) {
    return TimeSpan(_seconds-right._seconds);
    }


    // ----------- EspRTC Anfang

    struct EspRTC::SPrivateData
    {
        uRTCLib rtc;
        bool initialized = false;
    };

    EspRTC::EspRTC()
        : mData(new SPrivateData())
    {
        Wire.begin();
        // Only used once, then disabled
        //  rtc.set(0, 46, 21, 6, 3, 5, 19);
        //  RTCLib::set(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year)
        // Initialize SD card
        mData->rtc.set_rtc_address(0x68);
        // mData->rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
        Serial.println("RTC initialisiert!");

        mData->initialized = true;
    }

    EspRTC::~EspRTC() 
    {
    }

    DateTime EspRTC::getCurrentTime()
    {
        if(!mData->initialized)
        {
            Serial.println("RTC ist nicht initalisiert! Gebe zufaelliges Datum aus...");

            uint8_t hour = esp_random() % 24;
            uint8_t minute = esp_random() % 60;
            uint8_t second = esp_random() % 60;

            DateTime ret(2019, 4, 8, hour, minute, second);

            return ret;
        }

        mData->rtc.refresh();
        DateTime ret(mData->rtc.year(), mData->rtc.month(), mData->rtc.day(), mData->rtc.hour(), mData->rtc.minute(), mData->rtc.second());

        return ret;
    }

    String EspRTC::dateTimeToString(const DateTime& input)
    {
        String date;

        date += fillWithZeros(String(input.year()), 4);
        date += "-";
        date += fillWithZeros(String(input.month()), 2); 
        date += "-";
        date += fillWithZeros(String(input.day()), 2); 
        date += "T";
        date += fillWithZeros(String(input.hour()), 2);
        date += ":";
        date += fillWithZeros(String(input.minute()), 2);
        date += ":";
        date += fillWithZeros(String(input.second()), 2);
        date += "Z";
        return date;
    }

    EspRTC& EspRTC::Instance()
    {
        static EspRTC ret;

        return ret;
    }
}
