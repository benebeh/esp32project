#include "ShortTermOutputHandler.h"
#include "SDHandle.h"

namespace model
{
	void ShortTermOutputHandler::write() const
	{
		SDHandle::Instance().saveData((String) output(), createFilePath("shortTerm") + createFileName("csv", "longtermData_"), false);
	}
}

